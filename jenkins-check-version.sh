#!/bin/bash -xe

set -exuo pipefail

LOCAL_VERSION=$(node -p -e "require('./package.json').version"); # Version in package.json
REMOTE_VERSION=$(npm view . version); # Version was published
# echo $LOCAL_VERSION
# echo $REMOTE_VERSION

if [ "${LOCAL_VERSION}" == "${REMOTE_VERSION}" ] || [ "${LOCAL_VERSION}" \< "${REMOTE_VERSION}" ]
then # if/then branch
    echo 0;
else # else branch
	echo 1;
fi
